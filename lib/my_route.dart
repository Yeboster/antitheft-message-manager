import 'package:flutter/material.dart';

import 'my_app_meta.dart' as my_app_meta;

abstract class MyRoute extends StatelessWidget {
  const MyRoute();

  static of(BuildContext context) =>
      context.rootAncestorStateOfType(const TypeMatcher<MyRoute>());

  String get routeName => '/${this.runtimeType.toString()}';

  String get title => this.routeName;

  String get description => null;

  Widget buildTheBody(BuildContext context);

  @override
  Widget build(BuildContext context) {
    print(routeName);
    return Scaffold(
      appBar: AppBar(
        title: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(title),
        ),
        actions: _getAppBarActions(),
      ),
      body: Builder(builder: (BuildContext context) {
        return Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 28.0),
            child: buildTheBody(context),
          ),
        );
      }),
    );
  }

  List<Widget> _getAppBarActions() {
    final actionBarActions = <Widget>[];

    actionBarActions.add(
      PopupMenuButton(itemBuilder: (BuildContext context) {
        return _getPopupItems(context);
      }),
    );

    return actionBarActions;
  }

  List<PopupMenuItem> _getPopupItems(BuildContext context) {
    final popupItems = <PopupMenuItem>[];
    if (routeName != my_app_meta.kConfigRouteName) {
      popupItems.add(
        PopupMenuItem(
          child: ListTile(
              title: Text(my_app_meta.kConfigName),
              onTap: () => Navigator.of(context).pushNamedAndRemoveUntil(
                  my_app_meta.kConfigRouteName,
                  (Route<dynamic> route) => false)),
        ),
      );
    }
    if (routeName != my_app_meta.kAboutRouteName) {
      popupItems.add(
        PopupMenuItem(
          child: ListTile(
            title: Text(my_app_meta.kAboutName),
            onTap: () =>
                Navigator.pushNamed(context, my_app_meta.kAboutRouteName),
          ),
        ),
      );
    }
    return popupItems;
  }
}
