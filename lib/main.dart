import 'package:flutter/material.dart';

import 'my_app_meta.dart' as my_app_meta;
import 'package:permission_handler/permission_handler.dart';

void main() => runApp(AntitheftManager());

class AntitheftManager extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Note: this method returns a Future type, so need to treat with .then() function
    PermissionHandler()
        .checkPermissionStatus(PermissionGroup.sms)
        .then((value) {
      if (value != PermissionStatus.granted) {
        PermissionHandler().requestPermissions([PermissionGroup.sms]);
      }
    });

    return MaterialApp(
      title: my_app_meta.APP_NAME,
      theme: ThemeData(primaryColor: Colors.red, accentColor: Colors.red),
      initialRoute: '/',
      routes: my_app_meta.kRoutingTable,
    );
  }
}
