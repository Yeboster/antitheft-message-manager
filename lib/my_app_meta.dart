import 'package:flutter/material.dart';

import './my_route.dart';
import 'widget/about.dart';
import 'widget/code.dart';
import 'widget/configuration.dart';
import 'widget/group.dart';
import 'widget/secure.dart';
import 'widget/feedback.dart';

const APP_VERSION = 'v0.8';
const APP_NAME = 'Antifurto manager';
final kAppIcon =
    Image.asset('res/images/launcher_icon.png', height: 64.0, width: 64.0);
const APP_DESCRIPTION = "Un'applicazione per permettere la gestione"
    " delle comunicazioni del proprio sistema antifurto."
    '\n\nSviluppato da Yeboster.';

const kHomeRouteName = kCodeRouteName;
const kHomeName = kCodeName;
const kHomeRoute = kCodeRoute;

const kConfigRouteName = '/Configuration';
const kConfigName = 'Configurazione';
const kConfigRoute = ConfigurationRoute();

const kCodeRouteName = '/Code';
const kCodeName = 'Codice segreto';
const kCodeRoute = CodeRoute();

const kGroupRouteName = '/Group';
const kGroupName = "Gruppo dell'impianto";
const kGroupRoute = GroupRoute();

const kSecureRouteName = '/Secure';
const kSecureName = "Modalità dell'impianto";
const kSecureRoute = SecureRoute();

const kFeedbackRouteName = '/Feedback';
const kFeedbackName = "Feedback";
const kFeedbackRoute = FeedbackRoute();

const kAboutRouteName = '/About';
const kAboutName = 'Informazioni';
const kAboutRoute = AboutRoute();

class Route {
  const Route(this.name, this.routeName, this.route);

  final String name;
  final String routeName;
  final MyRoute route;
}

const kMyAppRoutesStructure = <Route>[
  Route(kConfigName, kConfigRouteName, kConfigRoute),
  Route(kCodeName, kCodeRouteName, kCodeRoute),
  Route(kGroupName, kGroupRouteName, kGroupRoute),
  Route(kSecureName, kSecureRouteName, kSecureRoute),
  Route(kFeedbackName, kFeedbackRouteName, kFeedbackRoute),
];

// Mapping route names to routes.
final kRouteNameToRouteMap = Map<String, MyRoute>.fromIterable(
  kMyAppRoutesStructure,
  key: (route) => route.routeName,
  value: (route) => route.route,
);

Map<String, WidgetBuilder> kRoutingTable = kRouteNameToRouteMap.map(
  (routeName, route) {
    final widgetBuilder = (BuildContext context) => route;
    return MapEntry<String, WidgetBuilder>(routeName, widgetBuilder);
  },
)..addAll(
    {
      // By default go to home screen. (Navigator.defaultRouteName is just '/')
      Navigator.defaultRouteName: (context) => kHomeRoute,
      kHomeRouteName: (context) => kHomeRoute,
      kAboutRoute.routeName: (context) => kAboutRoute,
    },
  );
