class Configuration {
  String systemName, systemType, phone;

  Configuration({this.systemName, this.systemType, this.phone});

  Map<String, dynamic> toJson() => {
        'systemName': this.systemName,
        'systemType': this.systemType,
        'phone': this.phone,
      };

  Configuration.fromJson(Map<String, dynamic> json)
      : systemName = json['systemName'],
        systemType = json['systemType'],
        phone = json['phone'];

  bool isValid() {
    return systemName != null && systemType != null && phone != null;
  }
}
