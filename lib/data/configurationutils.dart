import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'configuration.dart';

class ConfigurationUtils {
  ConfigurationUtils();

  final String keyConfJson = 'configurationJSON';

  Configuration createConfFromJson(String jsonString) {
    Configuration configuration;
    try {
      Map confMap = jsonDecode(jsonString);
      configuration = new Configuration.fromJson(confMap);
    } catch (e) {
      configuration = new Configuration();
    }
    return configuration;
  }

  saveJson(Map<String, dynamic> confJson) {
    final String json = jsonEncode(confJson);
    writeJson(json);
  }

  writeJson(String json) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(keyConfJson, json);
  }

  Future<String> getJson() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyConfJson);
  }
}
