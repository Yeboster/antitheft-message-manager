class SecureData {
  static const String ON = 'Acceso';
  static const String OFF = 'Spento';
  static const String INT = 'Interno';
  static const String PAR = 'Parziale';

  static SecureEnum parser(String val) {
    SecureEnum ret;
    switch (val.toLowerCase()) {
      case 'acceso':
        ret = SecureEnum.ON;
        break;
      case 'spento':
        ret = SecureEnum.OFF;
        break;
      case 'interno':
        ret = SecureEnum.INT;
        break;
      case 'parziale':
        ret = SecureEnum.PAR;
        break;
    }
    return ret;
  }

  static String enumToString(SecureEnum secureEnum) {
    return secureEnum.toString().split('.').last;
  }
}

enum SecureEnum { ON, OFF, INT, PAR }
