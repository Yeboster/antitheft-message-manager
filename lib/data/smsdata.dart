import 'secure.dart';
import 'package:flutter_sms/flutter_sms.dart';

class SmsData {
  SmsData(
      {this.systemName,
      this.systemType,
      this.phone,
      this.code,
      this.group,
      this.ack,
      this.secure});

  String systemName, systemType, phone, code, group;
  bool ack;
  SecureEnum secure;

  static String smsCodeFormatter(SmsData smsData) {
    return "#${smsData.code}#${SecureData.enumToString(smsData.secure)}G${smsData.group}${smsData.ack ? '?' : ''}";
  }

  static void sendSMS(String message, List<String> recipients) async {
    String prefix = '+39';
    List<String> prefixRecipients =
        recipients.map((contact) => prefix + contact).toList();
    print(prefixRecipients);
    String _result =
        await FlutterSms.sendSMS(message: message, recipients: prefixRecipients)
            .catchError((onError) {
      print(onError);
    });
    print(_result);
  }
}
