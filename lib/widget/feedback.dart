import 'package:flutter/material.dart';

import '../data/smsdata.dart';
import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';

class FeedbackRoute extends MyRoute {
  const FeedbackRoute({this.smsData});

  final SmsData smsData;

  @override
  String get routeName => my_app_meta.kFeedbackRouteName;

  @override
  String get title => my_app_meta.kFeedbackName;

  @override
  Widget buildTheBody(BuildContext context) => Feedback(smsData);
}

class Feedback extends StatefulWidget {
  const Feedback(this.smsData);

  final SmsData smsData;

  @override
  State createState() => new _FeedbackState(smsData);
}

class _FeedbackState extends State<Feedback> {
  _FeedbackState(this._smsData);

  final _feedbackFormKey = GlobalKey<FormState>();
  SmsData _smsData;

  bool _ackState = true;

  @override
  void initState() {
    super.initState();
    if (_smsData == null) {
      _smsData = new SmsData();
    }
    if (_smsData.secure == null) {
      // Taking the group from the Group widget
      Navigator.pushReplacementNamed(context, my_app_meta.kSecureRouteName);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _textTheme = Theme.of(context).textTheme;
    return Form(
      key: _feedbackFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 18.0),
          Text(
            "Si vuole ricevere il messaggio di conferma?",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          SwitchListTile(
            title: Text("Messaggio di conferma"),
            value: _ackState,
            onChanged: (value) {
              setState(() {
                _ackState = !_ackState;
              });
            },
          ),
          SizedBox(
            height: 24.0,
          ),
          RaisedButton(
            color: Colors.red,
            child: Text(
              'Invia',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            onPressed: () {
              _smsData.ack = _ackState;
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text('Invio manuale necessario'),
              ));
              print(_smsData.phone);
              String message = SmsData.smsCodeFormatter(_smsData);
              SmsData.sendSMS(message, [_smsData.phone]);
            },
          )
        ],
      ),
    );
  }
}
