import 'package:flutter/material.dart';

import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';

class AboutRoute extends MyRoute {
  const AboutRoute();

  @override
  String get title => my_app_meta.kAboutName;

  @override
  String get routeName => my_app_meta.kAboutRouteName;

  @override
  Widget buildTheBody(BuildContext context) {
    final TextTheme _textTheme = Theme.of(context).textTheme;

    return Column(
      children: <Widget>[
        Icon(
            Icons.laptop,
          size: 88.0,
        ),
    SizedBox(height: 50.0),
    Text(
          my_app_meta.APP_NAME,
          textAlign: TextAlign.center,
          style: _textTheme.headline,
        ),
        SizedBox(height: 30.0),
        Text(
          my_app_meta.APP_DESCRIPTION,
          textAlign: TextAlign.center,
          style: _textTheme.subhead,
        )
      ],
    );
  }
}