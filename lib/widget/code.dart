import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../data/configuration.dart';
import '../data/configurationutils.dart';
import '../data/smsdata.dart';
import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';
import 'configuration.dart';
import 'group.dart';

class CodeRoute extends MyRoute {
  const CodeRoute({Key key, this.smsData});

  final SmsData smsData;

  @override
  String get title => my_app_meta.kCodeName;

  @override
  String get routeName => my_app_meta.kCodeRouteName;

  @override
  Widget buildTheBody(BuildContext context) => Code(smsData);
}

class Code extends StatefulWidget {
  Code(this.smsData);

  final SmsData smsData;

  @override
  State createState() => _CodeState(smsData);
}

class _CodeState extends State<Code> {
  _CodeState(this._smsData);

  final _codeFormKey = GlobalKey<FormState>();
  String _code;
  SmsData _smsData;

  String _codeValidator(String code) =>
      code.length < 6 ? 'Il codice inserito non è corretto' : null;

  @override
  void initState() {
    super.initState();
    if (_smsData == null) {
      _smsData = new SmsData();
    }
    if (_smsData.phone == null || _smsData.systemType == null) {
      // Take phoneNumber from the file
      ConfigurationUtils configurationUtils = new ConfigurationUtils();
      configurationUtils.getJson().then((json) {
        Configuration conf = configurationUtils.createConfFromJson(json);
        if (conf != null &&
            conf.phone != null &&
            conf.phone.isNotEmpty &&
            conf.systemType != null &&
            conf.systemType.isNotEmpty &&
            conf.systemName != null &&
            conf.systemName.isNotEmpty) {
          _smsData.phone = conf.phone;
          _smsData.systemType = conf.systemType;
          setState(() {
            _smsData.systemName = conf.systemName;
          });
        } else {
          // If does not exist, init configuration again.
          Navigator.pushReplacement(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext context) => new ConfigurationRoute()));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme _textTheme = Theme.of(context).textTheme;

    return Form(
      key: _codeFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 18.0),
          Text(
            "Inserisci il codice segreto utente per l'impianto: ${_smsData.systemName != null ? _smsData.systemName : ''}",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          TextFormField(
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                filled: true,
                icon: Icon(Icons.vpn_key),
                labelText: 'Codice numerico'),
            keyboardType: TextInputType.phone,
            obscureText: true,
            onSaved: (value) {
              this._code = value;
            },
            validator: (value) => _codeValidator(value),
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
            ],
            maxLength: 6,
          ),
          SizedBox(height: 24.0),
          RaisedButton(
            color: Colors.red,
            child: Text(
              'Continua',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            onPressed: () {
              if (_codeFormKey.currentState.validate()) {
                // Save the form
                _codeFormKey.currentState.save();
                _smsData.code = _code;

                // Navigate to group
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new GroupRoute(smsData: _smsData)));
              }
            },
          ),
        ],
      ),
    );
  }
}
