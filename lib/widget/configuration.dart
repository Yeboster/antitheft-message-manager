import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../data/configuration.dart';
import '../data/configurationutils.dart';
import '../data/smsdata.dart';
import '../data/systemtype.dart';
import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';
import '../widget/code.dart';

class ConfigurationRoute extends MyRoute {
  const ConfigurationRoute();

  @override
  String get title => my_app_meta.kConfigName;

  @override
  String get routeName => my_app_meta.kConfigRouteName;

  @override
  Widget buildTheBody(BuildContext context) => ConfigurationForm();
}

class ConfigurationForm extends StatefulWidget {
  final ConfigurationUtils _configurationUtils = new ConfigurationUtils();
  final String phoneNumber = '';

  @override
  _ConfigurationFormState createState() => _ConfigurationFormState();
}

class _ConfigurationFormState extends State<ConfigurationForm> {
  Configuration _configuration;
  String _selectedAntiTheftSystemType = SystemType.antiTheftSystemType.first;
  final _configurationFormKey = GlobalKey<FormState>();
  static const String _phoneNumberPrefix = '+39';
  final List<DropdownMenuItem<String>> _dropDownAntiTheftSystemType =
      SystemType.antiTheftSystemType
          .map((String value) => new DropdownMenuItem<String>(
                value: value,
                child: new Text(value),
              ))
          .toList();

  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _systemNameController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    widget._configurationUtils.getJson().then((String json) {
      if (json != null && json.isNotEmpty) {
        _configuration = widget._configurationUtils.createConfFromJson(json);
        setState(() {
          _phoneController.text = _configuration.phone.substring(3);
          _systemNameController.text = _configuration.systemName;
          if (_configuration.systemType != null) {
            _selectedAntiTheftSystemType = _configuration.systemType;
          }
        });
      } else {
        _configuration = new Configuration();
      }
    });
  }

  String _phoneValidator(String num) {
    return num.length < 10 ? 'Il numero inserito non è corretto' : null;
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme _textTheme = Theme.of(context).textTheme;

    return Form(
      key: _configurationFormKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 18.0),
          Text(
            "Inserisci il numero di telefono dell'impianto antifurto *",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          TextFormField(
            controller: _phoneController,
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                filled: true,
                icon: Icon(Icons.phone),
                labelText: 'Numero di telefono',
                prefixText: _phoneNumberPrefix),
            keyboardType: TextInputType.phone,
            onSaved: (value) {
              setState(() {
                _phoneController.text = value;
              });
            },
            validator: (value) => _phoneValidator(value),
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
            ],
            maxLength: 10,
          ),
          SizedBox(height: 40.0),
          // AntiTheft type
          Text(
            "Seleziona la marca dell'impianto antifurto",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          ListTile(
            leading: Icon(Icons.security),
            title: DropdownButton<String>(
              isExpanded: true,
              value: _selectedAntiTheftSystemType,
              onChanged: ((String newValue) {
                setState(() {
                  _selectedAntiTheftSystemType = newValue;
                });
              }),
              items: _dropDownAntiTheftSystemType,
            ),
          ),
          SizedBox(height: 40.0),
          Text(
            "Inserisci un nome del sistema (opzionale)",
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          TextFormField(
            controller: _systemNameController,
            decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                filled: true,
                icon: Icon(Icons.assignment_ind),
                labelText: 'Nome del sistema'),
            keyboardType: TextInputType.text,
            onSaved: (String value) {
              setState(() {
                _systemNameController.text =
                    value.isEmpty ? _selectedAntiTheftSystemType : value;
              });
            },
            maxLength: 18,
          ),
          SizedBox(height: 24.0),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: RaisedButton(
              color: Colors.red,
              child: Text(
                'Salva',
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              onPressed: () {
                if (_configurationFormKey.currentState.validate()) {
                  showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Conferma il numero'),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              Text(_phoneNumberPrefix + _phoneController.text),
                              Text('È corretto?'),
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('Conferma'),
                            onPressed: () {
                              // Save the form
                              _configurationFormKey.currentState.save();
                              _configuration.phone =
                                  _phoneNumberPrefix + _phoneController.text;
                              _configuration.systemType = _selectedAntiTheftSystemType;
                              _configuration.systemName = _systemNameController.text;

                              // Save phone, systemName and systemType on file
                              widget._configurationUtils.saveJson(_configuration.toJson());

                              // Create the smsData object and navigate to new widget page
                              SmsData smsData = new SmsData(
                                  phone: _configuration.phone,
                                  systemType: _selectedAntiTheftSystemType,
                                  systemName: _configuration.systemName);
                              if (Navigator.canPop(context)) {
                                Navigator.of(context).pushAndRemoveUntil(
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                        new CodeRoute(smsData: smsData)),
                                        (Route<dynamic> route) => false);
                              } else {
                                Navigator.pushReplacement(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                        new CodeRoute(smsData: smsData)));
                              }
                            },
                          ),
                          FlatButton(
                            child: Text('Annulla'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }
                          ),
                        ],
                      );
                    }
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
