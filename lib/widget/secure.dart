import 'package:flutter/material.dart';

import '../data/smsdata.dart';
import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';
import '../data/secure.dart';
import '../widget/feedback.dart';

class SecureRoute extends MyRoute {
  const SecureRoute({this.smsData});

  final SmsData smsData;

  @override
  String get routeName => my_app_meta.kSecureRouteName;

  @override
  String get title => my_app_meta.kSecureName;

  @override
  Widget buildTheBody(BuildContext context) => Secure(smsData);
}

class Secure extends StatefulWidget {
  const Secure(this.smsData);

  final SmsData smsData;

  @override
  State createState() => new _SecureState(smsData);
}

class _SecureState extends State<Secure> {
  _SecureState(this._smsData);

  final _secureFormKey = GlobalKey<FormState>();
  SmsData _smsData;

  String _secureChoice = SecureData.ON;

  void _handlerRadio(String choice) {
    setState(() {
      _secureChoice = choice;
    });
  }

  @override
  void initState() {
    super.initState();
    if (_smsData == null) {
      _smsData = new SmsData();
    }
    if (_smsData.group == null) {
      // Taking the group from the Group widget
      Navigator.pushReplacementNamed(context, my_app_meta.kGroupRouteName);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _textTheme = Theme.of(context).textTheme;
    return Form(
      key: _secureFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 18.0),
          Text(
            "Selezionare la modalità del sistema che si desidera attivare",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          RadioListTile(
            title: Text(SecureData.ON),
            value: SecureData.ON,
            groupValue: _secureChoice,
            onChanged: (value) => _handlerRadio(value),
          ),
          RadioListTile(
            title: Text(SecureData.INT),
            value: SecureData.INT,
            groupValue: _secureChoice,
            onChanged: (value) => _handlerRadio(value),
          ),
          RadioListTile(
            title: Text(SecureData.PAR),
            value: SecureData.PAR,
            groupValue: _secureChoice,
            onChanged: (value) => _handlerRadio(value),
          ),
          RadioListTile(
            title: Text(SecureData.OFF),
            value: SecureData.OFF,
            groupValue: _secureChoice,
            onChanged: (value) => _handlerRadio(value),
          ),
          RaisedButton(
            color: Colors.red,
            child: Text(
              'Continua',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            onPressed: () {
              _smsData.secure = SecureData.parser(_secureChoice);
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new FeedbackRoute(smsData: _smsData)));
            },
          )
        ],
      ),
    );
  }
}
