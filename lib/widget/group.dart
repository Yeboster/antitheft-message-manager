import 'package:flutter/material.dart';

import '../data/smsdata.dart';
import '../data/systemtype.dart';
import '../my_app_meta.dart' as my_app_meta;
import '../my_route.dart';
import 'secure.dart';

class GroupRoute extends MyRoute {
  const GroupRoute({this.smsData});

  final SmsData smsData;

  @override
  String get title => my_app_meta.kGroupName;

  @override
  String get routeName => my_app_meta.kGroupRouteName;

  @override
  Widget buildTheBody(BuildContext context) => Group(smsData);
}

class Group extends StatefulWidget {
  const Group(this.smsData);

  final SmsData smsData;

  @override
  State createState() => new _GroupState(smsData);
}

class _GroupState extends State<Group> {
  _GroupState(this._smsData);

  final _groupFormKey = GlobalKey<FormState>();
  SmsData _smsData;

  String _selectedGroupNumber = GroupType.groupNumber.first;
  final List<DropdownMenuItem<String>> _dropDownGroupNumber =
      GroupType.groupNumber
          .map((String value) => new DropdownMenuItem<String>(
                value: value,
                child: new Text(value),
              ))
          .toList();

  @override
  void initState() {
    super.initState();
    if (_smsData == null) {
      _smsData = new SmsData();
    }
    if (_smsData.code == null) {
      // Taking the code from the Code widget
      Navigator.pushReplacementNamed(context, my_app_meta.kCodeRouteName);
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme _textTheme = Theme.of(context).textTheme;

    // TODO: delete From since Group can not have an empty choice
    return Form(
      key: _groupFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 18.0),
          Text(
            "Seleziona il numero del gruppo da utilizzare",
            textAlign: TextAlign.center,
            style: _textTheme.subhead,
          ),
          SizedBox(height: 24.0),
          ListTile(
            leading: Icon(Icons.group),
            title: DropdownButton<String>(
              isExpanded: true,
              value: _selectedGroupNumber,
              onChanged: ((String newValue) {
                setState(() {
                  _selectedGroupNumber = newValue;
                });
              }),
              items: _dropDownGroupNumber,
            ),
          ),
          SizedBox(height: 24.0),
          RaisedButton(
            color: Colors.red,
            child: Text(
              'Continua',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
            onPressed: () {
              if (_groupFormKey.currentState.validate()) {
                // Save the form
                _groupFormKey.currentState.save();
                _smsData.group = _selectedGroupNumber;

                // Navigate to Secure
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new SecureRoute(smsData: _smsData)));
              }
            },
          ),
        ],
      ),
    );
  }
}
